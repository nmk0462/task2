from django.db import models
from datetime import datetime
# Create your models here.

from ..accounts.models import users


class posts(models.Model):
    label_options=(
        ('Silver','Silver'),
        ('Gold','Gold'),
        ('Platinum','Platinum'),
        
    )
    content=models.CharField(max_length=100)
    postedBy=models.ForeignKey(users,on_delete=models.CASCADE)
    label=models.CharField(max_length=100,default='Silver',choices=label_options)
    def __str__(self):
        return str(self.id)


class comments(models.Model):
    comment=models.CharField(max_length=100)
    commentOn=models.ForeignKey(posts,on_delete=models.CASCADE)
 

class personalDetails(models.Model):
    Add_Options = (
        ('Y', 'Yes'),
        ('N', 'No'),
    
    )
    username=models.CharField(max_length=100)
    hobbies=models.CharField(max_length=100)
    jobDetails=models.CharField(max_length=100)
    address=models.CharField(max_length=100)
    add=models.CharField(max_length=1, choices=Add_Options,default='N')
    
    def __str__(self):
        return self.username



class familyMembers(models.Model):
    name=models.CharField(max_length=100)
    relation_To=models.ForeignKey(personalDetails,on_delete=models.CASCADE,default=None)
    relation=models.CharField(max_length=100)
    age=models.IntegerField() 
    


