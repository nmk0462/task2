from .models import posts,comments,personalDetails,familyMembers
from .serializers import postsSerializer,commentsSerializer,personalDetailsSerializer,familyDetailsSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from datetime import date
import datetime
from ..accounts.models import users
from ..accounts.serializers import usersSerializer


class PostsViewSet(viewsets.ModelViewSet):
    queryset=posts.objects.all()
    serializer_class = postsSerializer
    def list(self, request):
        dataPosts=posts.objects.all()
        
        for i in dataPosts:
            commentData=comments.objects.filter(commentOn=i)
            if len(commentData)<3:
                i.label='Silver'
            elif len(commentData)<6:
                i.label='Gold'
            else:
                i.label='Platinum'           
            i.save()

        serializer = postsSerializer(dataPosts,many=True)


        return Response(serializer.data)

    @action(methods=['GET', 'POST'],detail=False)
    def comments(self,request):
        queryset=comments.objects.all()
        serializer_class=commentsSerializer
        def create_comment(request, serializer_class, model_class):
            request_data = request.data.copy() if not isinstance(request, dict) else request
            data_id = request_data.pop('id', None)
            if data_id:
                data_obj = model_class.objects.get(id=data_id)
                serializer = serializer_class(instance=data_obj, data=request_data, partial=True)
            else:
                serializer = serializer_class(data=request_data)
            serializer.is_valid(raise_exception=True)
            update_object = serializer.save()
            return serializer_class(instance=update_object).data

        if request.method=='GET':
            queryset=comments.objects.all()
            request
            return Response(commentsSerializer(queryset, many=True).data)
        else:
            return Response(create_comment(request, commentsSerializer, comments))
    



    
 


class personalDetailsViewSet(viewsets.ModelViewSet):
    queryset=personalDetails.objects.all()
    serializer_class = personalDetailsSerializer
    finalResult=[]
    def list(self,request,pk=None):
        details=personalDetails.objects.all()
        result=[]
        finalResult=[]
        for i in details:
            details1=personalDetails.objects.filter(username=i.username)
            if i.add=='Y':
                previousData=users.objects.filter(name=i.username)
                serializer2=usersSerializer(previousData,many=True)
                serializer1=personalDetailsSerializer(details1,many=True)
                familyData=familyMembers.objects.filter(relation_To=details1[0])
                serializer3=familyDetailsSerializer(familyData,many=True)

                result=serializer2.data+serializer1.data+serializer3.data
            else:
                serializer1=personalDetailsSerializer(details1,many=True)

                result=serializer1.data
            finalResult=finalResult+result
        
        return Response(finalResult)
    @action(methods=['GET', 'POST'],detail=False)
    def family(self,request):
        queryset=familyMembers.objects.all()
        serializer_class=familyDetailsSerializer
        def create_family(request, serializer_class, model_class):
            request_data = request.data.copy() if not isinstance(request, dict) else request
            data_id = request_data.pop('id', None)
            if data_id:
                data_obj = model_class.objects.get(id=data_id)
                serializer = serializer_class(instance=data_obj, data=request_data, partial=True)
            else:
                serializer = serializer_class(data=request_data)
            serializer.is_valid(raise_exception=True)
            update_object = serializer.save()
            return serializer_class(instance=update_object).data

        if request.method=='GET':
            queryset=familyMembers.objects.all()
            
            return Response(familyDetailsSerializer(queryset, many=True).data)
        else:
            return Response(create_family(request, familyDetailsSerializer, comments))
    
