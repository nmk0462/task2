from rest_framework import serializers
from rest_framework.serializers import ValidationError
from . models import posts,comments,personalDetails,familyMembers
from rest_framework.response import Response

from ..accounts.serializers import usersSerializer 
        

class postsSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        represent = super().to_representation(instance)
        represent['postedBy'] = usersSerializer(instance.postedBy).data
        return represent
    class Meta:
        model = posts
        fields = ('id','content','postedBy','label')
 



class commentsSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        represent = super().to_representation(instance)
        represent['commentOn'] = postsSerializer(instance.commentOn).data
        return represent
    class Meta:

        model = comments
        
        fields = ('id','comment','commentOn')
    


class personalDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = personalDetails 
        fields = ('id','username','hobbies','jobDetails','address','add')
    def validate_username(self,value):
        data=self.get_initial
        username=value
        l=personalDetails.objects.filter(username=username)
        if l.exists():
            raise ValidationError("Choose another name, already exists")
        return value



class familyDetailsSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        represent = super().to_representation(instance)
        represent['rellation_To'] = personalDetailsSerializer(instance.relation_To).data
        return represent
    class Meta:
        model = familyMembers 
        fields = ('id','name','relation','age','relation_To')


