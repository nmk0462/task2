from .models import users
from ..base.models import posts
from .serializers import usersSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from datetime import date
import datetime
class UserViewSet(viewsets.ModelViewSet):
    queryset=users.objects.all()
    serializer_class = usersSerializer
    def get_queryset(self):
        gender1 = self.request.query_params.get('gender1')
        age = self.request.query_params.get('age')
        
        queryset = super(UserViewSet, self).get_queryset()
        queryset=queryset.all()
        if gender1:
        
            queryset = users.objects.filter(gender=gender1)
        if age:
            current_time = datetime.datetime.now()
            year=current_time.year
            age=str((year-int(age)))+'-'+str(current_time.month)+'-'+str(current_time.day)
            queryset = queryset.filter(dateOfBirth__gt=age)


        return queryset
    @action(methods=['GET'],detail=False)
    def countusers(self,request):
        userset=users.objects.all()
        count=len(userset)
  
        return Response({'Number of Users':count})
    
    @action(methods=['GET'],detail=False)
    def numberofposts(Self,request):
        userset=users.objects.all()
        postsset=[]
        for i in userset:
            numberOfPosts=posts.objects.filter(postedBy=i.id)
            postsset.append([i.name,len(numberOfPosts)])
        
  
        return Response({'User,NumberOfPosts':postsset})



