from rest_framework import serializers
from rest_framework.serializers import ValidationError
from .models import users
from rest_framework.response import Response


class usersSerializer(serializers.ModelSerializer):
    class Meta:
        model = users
        fields = ('id','name','email','mobile','dateOfBirth','gender')
    def validate_name(self,value):
        data=self.get_initial
        name=value
        l=users.objects.filter(name=name)
        if l.exists():
            raise ValidationError("Choose another name, already exists")
        return value
    def validate_email(self,value):
        data=self.get_initial
        email=value
        l=users.objects.filter(email=email)
        if l.exists():
            raise ValidationError("Email already exists")
        return value
    def validate_mobile(self,value):
        data=self.get_initial
        mobile=value
        l=users.objects.filter(mobile=mobile)
        if l.exists():
            raise ValidationError("Mobile number already exists")
        return value


        

